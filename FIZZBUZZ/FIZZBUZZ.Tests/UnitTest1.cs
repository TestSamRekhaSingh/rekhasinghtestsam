﻿using System;
using System.Linq;
using FIZZBUZZ.BL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FIZZBUZZ.Tests
{
    [TestClass]
    public class FizzBuzzProcessor_UnitTest
    {
        FizzBuzzProcessor sut = new FizzBuzzProcessor(DateTime.Now);
        private int startNumber = 1;

        [TestMethod]
        public void WhenTestingGetFizzBuzzList_WithNegativeValue()
        {
            var actualFizzBuzzList = sut.GetFizzBuzzList(startNumber ,- 1);

            Assert.AreEqual(0, actualFizzBuzzList.Count());
        }

        [TestMethod]
        public void WhenTestingGetFizzBuzzList_WithZero()
        {
            var actualFizzBuzzList = sut.GetFizzBuzzList(startNumber,0);
            Assert.AreEqual(0, actualFizzBuzzList.Count());
        }

        [TestMethod]
        public void WhenTestingGetFizzBuzzList_WithPositive_LessThan1000()
        {
            var actualFizzBuzzList = sut.GetFizzBuzzList(startNumber, 999);
            Assert.AreEqual(999, actualFizzBuzzList.Count());
        }

        [TestMethod]
        public void WhenTestingGetFizzBuzzList_WithPositive_MoreThan1000()
        {
            var actualFizzBuzzList = sut.GetFizzBuzzList(startNumber, 1001);
            Assert.AreEqual(0, actualFizzBuzzList.Count());
        }

        [TestMethod]
        public void WhenTestingGetFizzBuzzList_WithPositive_WithDisplayValue()
        {
            var actualFizzBuzzList = sut.GetFizzBuzzList(startNumber, 20);
            Assert.AreEqual(20, actualFizzBuzzList.Count());
            Assert.AreEqual("1", actualFizzBuzzList[0].DisplayValue);
            Assert.AreEqual("Fizz", actualFizzBuzzList[2].DisplayValue);
        }

        [TestMethod]
        public void WhenTestingGetFizzBuzzList_WithPositive_WithDivideBy3Rule()
        {
            var actualFizzBuzzList = sut.GetFizzBuzzList(startNumber, 20);

            Assert.AreEqual(20, actualFizzBuzzList.Count());

            Assert.AreEqual("1", actualFizzBuzzList[0].DisplayValue);
            Assert.AreEqual("2", actualFizzBuzzList[1].DisplayValue);
            Assert.AreEqual("Fizz", actualFizzBuzzList[2].DisplayValue);
            Assert.AreEqual("4", actualFizzBuzzList[3].DisplayValue);
            Assert.AreEqual("Buzz", actualFizzBuzzList[4].DisplayValue);
            Assert.AreEqual("Fizz", actualFizzBuzzList[5].DisplayValue);
        }

        [TestMethod]
        public void WhenTestingGetFizzBuzzList_WithPositive_WithDivideBy3Rule_and_WithDivideBy5Rule()
        {
            var actualFizzBuzzList = sut.GetFizzBuzzList(startNumber, 20);

            Assert.AreEqual(20, actualFizzBuzzList.Count());

            Assert.AreEqual("1", actualFizzBuzzList[0].DisplayValue);
            Assert.AreEqual("2", actualFizzBuzzList[1].DisplayValue);
            Assert.AreEqual("Fizz", actualFizzBuzzList[2].DisplayValue);
            Assert.AreEqual("4", actualFizzBuzzList[3].DisplayValue);
            Assert.AreEqual("Buzz", actualFizzBuzzList[4].DisplayValue);
            Assert.AreEqual("Fizz", actualFizzBuzzList[5].DisplayValue);
            Assert.AreEqual("7", actualFizzBuzzList[6].DisplayValue);
            Assert.AreEqual("8", actualFizzBuzzList[7].DisplayValue);
            Assert.AreEqual("Fizz", actualFizzBuzzList[8].DisplayValue);
            Assert.AreEqual("Buzz", actualFizzBuzzList[9].DisplayValue);
        }

        [TestMethod]
        public void WhenTestingGetFizzBuzzList_WithPositive_WithDivideBy3Rule_and_WithDivideBy5Rule_and_WithDivideBy3and5Rule()
        {
            var actualFizzBuzzList = sut.GetFizzBuzzList(startNumber,20);

            Assert.AreEqual(20, actualFizzBuzzList.Count());

            Assert.AreEqual("1", actualFizzBuzzList[0].DisplayValue);
            Assert.AreEqual("2", actualFizzBuzzList[1].DisplayValue);
            Assert.AreEqual("Fizz", actualFizzBuzzList[2].DisplayValue);
            Assert.AreEqual("4", actualFizzBuzzList[3].DisplayValue);
            Assert.AreEqual("Buzz", actualFizzBuzzList[4].DisplayValue);
            Assert.AreEqual("Fizz", actualFizzBuzzList[5].DisplayValue);
            Assert.AreEqual("7", actualFizzBuzzList[6].DisplayValue);
            Assert.AreEqual("8", actualFizzBuzzList[7].DisplayValue);
            Assert.AreEqual("Fizz", actualFizzBuzzList[8].DisplayValue);
            Assert.AreEqual("Buzz", actualFizzBuzzList[9].DisplayValue);
            Assert.AreEqual("11", actualFizzBuzzList[10].DisplayValue);
            Assert.AreEqual("Fizz", actualFizzBuzzList[11].DisplayValue);
            Assert.AreEqual("13", actualFizzBuzzList[12].DisplayValue);
            Assert.AreEqual("14", actualFizzBuzzList[13].DisplayValue);
            Assert.AreEqual("Fizz Buzz", actualFizzBuzzList[14].DisplayValue);
            Assert.AreEqual("16", actualFizzBuzzList[15].DisplayValue);
        }

        [TestMethod]
        public void WhenTestingGetFizzBuzzList_With_FizzBlue_BuzzGreen()
        {
            var actualFizzBuzzList = sut.GetFizzBuzzList(startNumber,10);

            Assert.AreEqual(10, actualFizzBuzzList.Count());

            Assert.AreEqual(null, actualFizzBuzzList[0].colorCode);
            Assert.AreEqual(null, actualFizzBuzzList[1].colorCode);
            Assert.AreEqual("blue", actualFizzBuzzList[2].colorCode);
            Assert.AreEqual(null, actualFizzBuzzList[3].colorCode);
            Assert.AreEqual("green", actualFizzBuzzList[4].colorCode);
            Assert.AreEqual("blue", actualFizzBuzzList[5].colorCode);
            Assert.AreEqual(null, actualFizzBuzzList[6].colorCode);
            Assert.AreEqual(null, actualFizzBuzzList[7].colorCode);
            Assert.AreEqual("blue", actualFizzBuzzList[8].colorCode);
            Assert.AreEqual("green", actualFizzBuzzList[9].colorCode);
        }

        [TestMethod]
        public void WhenTestingGetFizzBuzzList_With_CurrentDayWednesday_FizzWizz_BuzzWuzz()
        {
            DateTime processDateTime= new DateTime(2016,01,20);



            sut = new FizzBuzzProcessor(processDateTime);
            var actualFizzBuzzList = sut.GetFizzBuzzList(startNumber,16);
          
            Assert.AreEqual(16, actualFizzBuzzList.Count());

            Assert.AreEqual("1", actualFizzBuzzList[0].DisplayValue);
            Assert.AreEqual("2", actualFizzBuzzList[1].DisplayValue);
            Assert.AreEqual("Wizz", actualFizzBuzzList[2].DisplayValue);
            Assert.AreEqual("4", actualFizzBuzzList[3].DisplayValue);
            Assert.AreEqual("Wuzz", actualFizzBuzzList[4].DisplayValue);
            Assert.AreEqual("Wizz", actualFizzBuzzList[5].DisplayValue);
            Assert.AreEqual("7", actualFizzBuzzList[6].DisplayValue);
            Assert.AreEqual("8", actualFizzBuzzList[7].DisplayValue);
            Assert.AreEqual("Wizz", actualFizzBuzzList[8].DisplayValue);
            Assert.AreEqual("Wuzz", actualFizzBuzzList[9].DisplayValue);
            Assert.AreEqual("11", actualFizzBuzzList[10].DisplayValue);
            Assert.AreEqual("Wizz", actualFizzBuzzList[11].DisplayValue);
            Assert.AreEqual("13", actualFizzBuzzList[12].DisplayValue);
            Assert.AreEqual("14", actualFizzBuzzList[13].DisplayValue);
            Assert.AreEqual("Wizz Wuzz", actualFizzBuzzList[14].DisplayValue);
            Assert.AreEqual("16", actualFizzBuzzList[15].DisplayValue);
        }

    }
}
