﻿using System.ComponentModel.DataAnnotations;

namespace FIZZBUZZ.Models
{
    public class FizzBuzzModel 
    {
        [Required(ErrorMessage = "Required")]
        [Range(1, 1000,ErrorMessage = "FizzBuzz Number should be positive number and  must be a between 1 to 1000")]          
        public int fizzBuzzNumber { get; set; }
    }
}