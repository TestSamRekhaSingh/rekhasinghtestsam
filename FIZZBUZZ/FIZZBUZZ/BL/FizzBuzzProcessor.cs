﻿using System;
using System.Collections.Generic;

namespace FIZZBUZZ.BL
{
    public interface IFizzBuzzProcessor
    {
        ProcessedFizzBuzz[] GetFizzBuzzList(int startNumber, int endNumber);
    }

    public class FizzBuzzProcessor : IFizzBuzzProcessor
    {
        private DateTime processDateTime;

        public FizzBuzzProcessor(DateTime processDateTime)
        {
            this.processDateTime = processDateTime;
        }

        public ProcessedFizzBuzz[] GetFizzBuzzList(int startNumber, int endNumber)
        {
            if (endNumber <= 0 || endNumber > 1000)
                return new ProcessedFizzBuzz[0];

            var processedFizzList = new List<ProcessedFizzBuzz>();

            for (var i = startNumber; i <= endNumber; i++)
            {
                var fizzBuzz = new ProcessedFizzBuzz();

                if (i % 3 == 0 && i % 5 == 0)
                    fizzBuzz.DisplayValue = "Fizz Buzz";
                else if (i % 3 == 0)
                {
                    fizzBuzz.DisplayValue = "Fizz";
                    fizzBuzz.colorCode = "blue";
                }

                else if (i % 5 == 0)
                {
                    fizzBuzz.DisplayValue = "Buzz";
                    fizzBuzz.colorCode = "green";
                }

                else
                    fizzBuzz.DisplayValue = i.ToString();

                processedFizzList.Add(fizzBuzz);
            }

            if (processDateTime.DayOfWeek == DayOfWeek.Wednesday)
            {
                foreach (var processedFizz in processedFizzList)
                {
                    processedFizz.DisplayValue = processedFizz.DisplayValue.Replace("F", "W");
                    processedFizz.DisplayValue = processedFizz.DisplayValue.Replace("B", "W");
                }
            }

            return processedFizzList.ToArray();
        }
    }
}