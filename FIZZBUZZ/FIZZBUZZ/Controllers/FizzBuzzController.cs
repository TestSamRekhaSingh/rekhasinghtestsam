﻿using System;
using System.Web.Mvc;
using FIZZBUZZ.BL;
using FIZZBUZZ.Models;

namespace FIZZBUZZ.Controllers
{
    public class FizzBuzzController : Controller
    {
        
        public ActionResult FizzBuzz()
        {
            if (ModelState.IsValid)
            {
                return View("FizzBuzz");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult FizzBuzz(FizzBuzzModel fizz)
        {
            if (ModelState.IsValid)
            {
                var processor = new FizzBuzzProcessor(DateTime.Now);
                ViewBag.FizzBuzzList = processor.GetFizzBuzzList(1, fizz.fizzBuzzNumber);

                return View("FizzBuzz", fizz);
            }
            return View();

        }

        [HttpPost]
        public ActionResult NextFizzBuzz(FizzBuzzModel fizz)
        {
            if (ModelState.IsValid)
            {
                var processor = new FizzBuzzProcessor(DateTime.Now);
                ViewBag.FizzBuzzList = processor.GetFizzBuzzList(1, fizz.fizzBuzzNumber);

                return View("FizzBuzz", fizz);
            }
            return View();
        }
    }
}
